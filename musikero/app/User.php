<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $guarded = [];

    protected $appends = ['fullname'];

  
    public function getFullnameAttribute()
    {
        $name = "";

        if($this->suffix)
            $name .= $this->last_name.", ";
        else
            $name .= $this->last_name.", ";

        $name .= $this->first_name." ";

        if($this->middle_name)
            $name .= $this->middle_name ." ". $this->suffix;

        return $name;
    }


      /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
