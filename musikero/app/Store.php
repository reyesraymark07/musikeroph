<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';
    protected $guarded = [];

    public function album()
    {
        return $this->belongsTo('App\Album','album_id','id');
    }
}
