<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $table = 'artists';
    protected $guarded = [];

    public function music(){
        return $this->hasOne('App\Music','artist_id','id');
    }
}
