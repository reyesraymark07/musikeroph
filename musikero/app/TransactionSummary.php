<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionSummary extends Model
{
    protected $table = 'transaction_summarries';
    protected $guarded = [];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction','id','transaction_id');
    }

    public function album()
    {
        return $this->belongsTo('App\Album','album_id','id');
    }
    
}
