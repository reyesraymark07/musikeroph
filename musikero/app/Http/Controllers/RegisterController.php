<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(){
        return view('component.register');
    }

    public function login(){
        return view('component.login');
    }


    public function saveRegistration(Request $request){
        $c = $request->reg;
        $validator = Validator::make($c, [
            'last_name'=>'required|max:100',
            'first_name'=>'required|max:100',
            'middle_name'=>'max:100',
            'suffix'=>'max:10',
            'phone'=>'required|unique:users|max:11',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation|max:50',
            'password_confirmation' => 'min:6|max:50',
            'email' => 'required|unique:users|max:225|email',
            'user_name' => 'required|unique:users|max:225'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'Error', 'message' => 'Incomplete form details', 'errors' => $validator->messages()], 200);
        }

        $k = User::create([
            'last_name'=>$c['last_name'],
            'first_name'=>$c['first_name'],
            'middle_name'=>$c['middle_name'],
            'suffix'=>$c['suffix'],
            'password'=>bcrypt($c['password']),
            'email'=>$c['email'],
            'phone'=>$c['phone'],
            'user_name'=>$c['user_name']
        ]);
    }

}
