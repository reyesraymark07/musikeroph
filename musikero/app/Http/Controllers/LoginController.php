<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        //  return $request->all();
        if (Auth::attempt(['user_name' => $request->user_name, 'password' => $request->password, 'status' => 1])) {
            return response()->json([
                'status' => 'success',
                'message' => ''
            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid Credentials.'
            ]);
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
