<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Store;
use App\Artist;
use App\Album;
use App\Music;

class AdminController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }


    public function showPage(){
        return view('component.admin');
    }

    public function generateRandomString($length = 5)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    public function saveStore(Request $request){
        $id = auth()->user()->id;
        $fileName="";
        if ($request->file) {
            $namex = $this->generateRandomString() . time();
            $fileName = $id . $namex . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path('storage/stores/logo'), $fileName);
        }
        $k="";
        if($fileName){
            $k = Store::create([
                'name'=>$request->storeName,
                'description'=>$request->storeDesc,
                'banner'=>$fileName
            ]);
        }

        if(!$fileName){
            $k = Store::create([
                'name'=>$request->storeName,
                'description'=>$request->storeDesc
            ]);
        }
        
        if($k){
                return response()->json([
                    'status' => 'ok',
                    'message' => 'Store has been successfully saved!'
                ]);
        }

    }
    public function saveArtist(Request $request){
        $id = auth()->user()->id;
        $fileName="";

        if ($request->file) {
            $namex = $this->generateRandomString() . time();
            $fileName = $id . $namex . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path('storage/artist/picture'), $fileName);
        }
       
        $k="";
        if($fileName){
            $k = Artist::create([
                'name'=>$request->artistName,
                'description'=>$request->artistDesc,
                'picture'=>$fileName
            ]);
        }

        if(!$fileName){
            $k = Artist::create([
                'name'=>$request->artistName,
                'description'=>$request->artistDesc
            ]);
        }
        
        if($k){
                return response()->json([
                    'status' => 'ok',
                    'message' => 'Artist has been successfully saved!'
                ]);
        }

    }

    public function saveAlbum(Request $request){
        //   return $request->all();
        $id = auth()->user()->id;
        $fileName="";

        if ($request->file) {
            $namex = $this->generateRandomString() . time();
            $fileName = $id . $namex . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path('storage/album/banner'), $fileName);
        }
        $k="";
        if($fileName){
            $k = Album::create([
                'name'=>$request->albumName,
                'description'=>$request->albumDesc,
                'store_id'=>$request->storeId,
                'price'=>$request->albumPrice,
                'banner'=>$fileName
            ]);
        }

        if(!$fileName){

            $k = Album::create([
                'name'=>$request->albumName,
                'price'=>$request->albumPrice,
                'store_id'=>$request->storeId,
                'description'=>$request->albumDesc
            ]);
        }
        
        if($k){
                return response()->json([
                    'status' => 'ok',
                    'message' => 'Album has been successfully saved!'
                ]);
        }

    }

    public function saveMusic(Request $request){
        $id = auth()->user()->id;
        $fileName="";

        if ($request->file) {
            $namex = $this->generateRandomString() . time();
            $fileName = $id . $namex . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path('storage/music'), $fileName);
        }
        $k="";
        if($fileName){
            $k = Music::create([
                'title'=>$request->title,
                'description'=>$request->description,
                'artist_id'=>$request->artist_id,
                'album_id'=>$request->album_id,
                'file'=>$fileName
            ]);
        }

        
        
        if($k){
                return response()->json([
                    'status' => 'ok',
                    'message' => 'Album has been successfully saved!'
                ]);
        }

    }

    public function loadStore(){
        return Store::all();

    }
    public function loadArtist(){
        return Artist::all();

    }

    public function loadAlbum(){
        return Album::all();

    }

    public function loadMusic(Request $request){
      
        $id =  $request->album_id;
        return Music::with('album','artist')
        ->where('album_id', $id)
        ->OrderBy('title','asc')
        ->get();
    
    }

    

}
