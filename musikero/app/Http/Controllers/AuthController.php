<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
class AuthController extends Controller
{
    public function landing(){

        $album = Album::where('status',1)
        ->with('music')
        ->orderBy('created_at','Desc')
        ->get();
        return view('component.home',[
            'album'=>$album
        ]);
    }

}
