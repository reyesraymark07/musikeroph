<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\TransactionSummary;
use Auth;
use App\Music;
class ShopperController extends Controller
{
    public function __construct()
    {
        $this->middleware('shopper');
    }

    public function addToCart(Request $request){
        $val =  $request->params['transaction'];
        $count = Transaction::where('status',0)
        ->where('user_id',$val['user_id'])
        ->count();
        
        $trans_id = "";
        if($count<1){
            $k = Transaction::create([
                'user_id'=>$val['user_id']
            ]);
            $trans_id = $k->id;
        }
        else{
            $trans_id =Transaction::where('status',0)
            ->Where('user_id',$val['user_id'])
            ->first();
            $trans_id =  $trans_id ->id;   
        }
        // return $trans_id ;
        $l ="";
            $l = TransactionSummary::create([
                'transaction_id'=>$trans_id,
                'album_id'=>$val['album_id'],
                'amount'=>$val['price'],
                'user_id'=>$val['user_id'],
            ]);
            
        if($l){
            return response()->json([
                'status' => 'ok',
                'message' => 'Successfully added to cart!'
            ]);
    
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Server error'
            ]);
        }

    }

    public function updateItem(Request $request){
        
        $id  = $request->params['transaction_id'];
        $mainid  = $request->params['main_id'];
        $t = TransactionSummary::where('id',$id)
        ->update([
            'status'=>2
        ]);

        if($t){
        $data = Transaction::with('transSummary.album.store')
            ->where('id',$mainid)
            ->first(); 

            $total = TransactionSummary::where('status',0)
            ->where('transaction_id',$mainid)->sum('amount');

            return response()->json([
                'status' => 'ok',
                'message' => 'Item has been removed.',
                'data' =>$data,
                'total' =>$total
            ]);
        }
    }
    public function loadMusicShopper(){

        $id = Auth::user()->id;
        $data = TransactionSummary::with('album')
            ->where('user_id',$id)
            ->where('status',1)
            ->get(); 
        return view('component.music',[
            'album'=>$data,
        ]);
    }

    public function loadPlayList(Request $request){
        $id =  $request->album_id;
        // return $id;
        return Music::with('album','artist')
        ->where('album_id', $id)
        ->OrderBy('title','asc')
        ->get();
    }

    public function checkout(Request $request){
        $main_Id= $request->params['transaction_id'];
        $k = TransactionSummary::where('transaction_id',$main_Id)
        ->where('status',0)
        ->update([
            'status'=>1
        ]);

        $m = Transaction::where('id',$main_Id)
        ->update([
            'status'=>1
        ]);

        if($k && $m){
            return response()->json([
                'status' => 'ok',
                'message' => 'Purchase successful!'
            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Purchase unsuccessful!'
            ]); 
        }
    }

    public function loadCart(Request $request){

        $id = $request->user_id;
        $count = Transaction::where('status',0)
        ->where('user_id',$id)
        ->count();

        $trans_id = "";
        if($count<1){
            return response()->json([
                'status' => 'ok',
                'message' => 'Cart is empty!'
            ]);
        }
        else{

            $trans_id =Transaction::where('status',0)
            ->Where('user_id',$id)
            ->first();
            $trans_id =  $trans_id ->id;
           
            $data = Transaction::with('transSummary.album.store')
            ->where('id',$trans_id)
            ->first(); 

            $total = TransactionSummary::where('status',0)
            ->where('transaction_id',$trans_id)->sum('amount');

            return response()->json([
                'status' => 'ok',
                'message' => 'Cart is not empty',
                'data' =>$data,
                'total' =>$total
            ]);

        }

    }


}
