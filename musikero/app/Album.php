<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'albums';
    protected $guarded = [];

    public function music(){
        return $this->hasMany('App\Music','album_id','id');
    }

    public function transSummary()
    {
        return $this->hasMany('App\TransactionSummary','album_id','id');
    }

    public function store()
    {
        return $this->hasMany('App\Store','id','store_id');
    }

}
