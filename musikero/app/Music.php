<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $table = 'musics';
    protected $guarded = [];

    public function album()
    {
        return $this->belongsTo('App\Album','album_id','id');
    }
    
    public function artist()
    {
        return $this->belongsTo('App\Artist','artist_id','id');
    }
}
