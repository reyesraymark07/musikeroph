<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $guarded = [];

    public function transSummary(){
        return $this->hasMany('App\TransactionSummary','transaction_id','id')->where('status',0)->where('is_cancel',0);
    }

    public function transSummaryPaid(){
        return $this->hasMany('App\TransactionSummary','transaction_id','id')->where('status',1)->where('is_cancel',0);
    }

}
