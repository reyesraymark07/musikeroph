<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_summarries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->integer('album_id');
            $table->integer('user_id');
            $table->decimal('amount');
            $table->tinyInteger('is_cancel')->default('0');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_summarries');
    }
}
