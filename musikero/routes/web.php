<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@landing')->name('landing');
Route::get('/register', 'RegisterController@register')->name('register');
Route::get('/login-user', 'RegisterController@login')->name('login-user');
Route::post('/register-user', 'RegisterController@saveRegistration');
Route::post('/add-to-cart', 'ShopperController@addToCart');

Route::get('/store', 'StoreController@landing');

Route::get('/login', 'LoginController@authenticate')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::get('/admin', 'AdminController@showPage')->name('admin');
Route::post('/save-store', 'AdminController@saveStore');
Route::post('/save-artist', 'AdminController@saveArtist');

Route::post('/save-album', 'AdminController@saveAlbum');

Route::get('/load-store', 'AdminController@loadStore');
Route::get('/load-artists', 'AdminController@loadArtist');
Route::get('/load-albums', 'AdminController@loadAlbum');

Route::post('/save-music', 'AdminController@saveMusic');
Route::get('/load-music', 'AdminController@loadMusic');

Route::get('/load-cart', 'ShopperController@loadCart');
Route::post('/update-item', 'ShopperController@updateItem');
Route::post('/checkout', 'ShopperController@checkout');

Route::get('/load-music-shopper', 'ShopperController@loadMusicShopper');

Route::get('/load-playlist', 'ShopperController@loadPlayList');