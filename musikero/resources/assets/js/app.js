
require('./bootstrap');

window.Vue = require('vue');
import VueSweetalert2 from 'vue-sweetalert2';


import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);
Vue.component('example-component', require('./component/ExampleComponent.vue'));
Vue.component('home', require('./component/layout/Home.vue'));
Vue.component('register', require('./component/layout/Register.vue'));
Vue.component('login', require('./component/layout/Login.vue'));
Vue.component('admin', require('./component/layout/Admin.vue'));
Vue.component('topnav', require('./component/layout/TopNav.vue'));
Vue.component('store', require('./component/layout/Store.vue'));
Vue.component('music', require('./component/layout/Music.vue'));

const app = new Vue({
    el: '#app'
});
