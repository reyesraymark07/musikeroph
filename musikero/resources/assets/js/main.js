require('./bootstrap');

window.Vue = require('vue');



Vue.component('example-component', require('./component/ExampleComponent.vue'));
Vue.component('home', require('./component/layout/Home.vue'));

const app = new Vue({
    el: '#app'
});
