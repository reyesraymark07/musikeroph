@extends('layouts.app')
@section('content')
@if(Auth::check())
<music :_album="{{$album->toJson()}} " :_auth="{{Auth::user()->toJson()}}"></music>
@else
<music :_album="{{$album->toJson()}} "></music>
@endif
@endsection