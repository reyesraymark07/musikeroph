@extends('layouts.app')
@section('content')
@if(Auth::check())
<store :_album="{{$album->toJson()}} " :_auth="{{Auth::user()->toJson()}}"></store>
@else
<store :_album="{{$album->toJson()}} "></store>
@endif
@endsection