@if(Auth::check())
<topnav :_auth="{{Auth::user()->toJson()}}"></topnav>
@else
<topnav></topnav>
@endif