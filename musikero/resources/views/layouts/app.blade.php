
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
 @include('layouts.header')
  <body>
    <div class="page" id="app" >
      <!-- Page Header-->
    @include('layouts.topnavbar')
    @yield('content')
    @include('layouts.footer')
    </div>
    @include('layouts.scripts')

	</body>
</html>