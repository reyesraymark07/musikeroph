<div class="preloader">
      <div class="preloader-logo"><img src="{{ asset('musc/images/logo-default-131x43.png')}}" alt="" width="131" height="43" srcset="{{ asset('musc/images/logo-default-282x86.png 2x')}}"/>
      </div>
      <div class="preloader-body">
        <div id="loadingProgressG">
          <div class="loadingProgressG" id="loadingProgressG_1"></div>
        </div>
      </div>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('musc/js/core.min.js')}}"></script>
    <script src="{{ asset('musc/js/script.js')}}"></script>
    
	
	<!--LIVEDEMO_00 -->