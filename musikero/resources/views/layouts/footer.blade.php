  <!-- Page Footer-->
            <footer class="section footer-advanced bg-secondary">
              <div class="footer-advanced-main">
                <div class="container">
                  <div class="row row-50">
                    <div class="col-lg-7">
                      <h4>About Us</h4>
                      <p class="footer-advanced-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</p>
                    </div>
                    <div class="col-sm-5 col-md-5 col-lg-5">
                      <h4>Recent Album Posts</h4>
                      <!-- Post Inline-->
                      <article class="post-inline">
                        <p class="post-inline-title"><a href="single-blog-post.html">Minim veniam, quis nostrud exercitation</a></p>
                        <ul class="post-inline-meta">
                          <li>by John Doe</li>
                          <li><a href="single-blog-post.html">2 days ago</a></li>
                        </ul>
                      </article>
                    
                       <article class="post-inline">
                        <p class="post-inline-title"><a href="single-blog-post.html">Minim veniam, quis nostrud exercitation</a></p>
                        <ul class="post-inline-meta">
                          <li>by John Doe</li>
                          <li><a href="single-blog-post.html">2 days ago</a></li>
                        </ul>
                      </article>
                    

                    </div>
                
                  </div>
                </div>
              </div>
             
              <div class="container">
                <hr>
              </div>
              <div class="footer-advanced-aside">
                <div class="container">
                  <div class="footer-advanced-layout"><a class="brand" href="index.html"><img src="../musc/images/logo-inverse-102x34.png" alt="" width="102" height="34" srcset="../musc/images/logo-inverse-204x68.png 2x"/></a>
                    <!-- Rights-->
                    <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>All Rights Reserved.</span><span>&nbsp;</span><br class="d-sm-none"/><a href="#">Raymark Reyes</a><span> and</span><span>&nbsp;</span><a href="#">Exam</a></p>
                  </div>
                </div>
              </div>
            </footer>