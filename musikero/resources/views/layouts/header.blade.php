 <head>
    <title>Home</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width height=device-height initial-scale=1.0 maximum-scale=1.0 user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Work+Sans:300,700,800%7CWork+Sans:300,400,500,600,700%7CPlayfair+Display:400,400i,700">
    <link rel="stylesheet" href="{{ asset('musc/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('musc/css/fonts.css')}}">
    <link rel="stylesheet" href="{{ asset('musc/css/style.css')}}" id="main-styles-link">
   <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
     <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>